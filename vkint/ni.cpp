// g++ -I/path/to/gsl ni.cpp -lgsl 
// quick and dirty code to compute the integrals in the introduction

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <assert.h> 
using namespace std;

#include <gsl/gsl_integration.h>

double func(double z, double zeta); 
double integral(double z) ; 
double my_integrand (double x, void *params);

int main (void) {

    cout.precision (18);
    cout.setf (ios::fixed, ios::floatfield);
   
    const double C1   = 0.968 ; 
    const double f    = 0.90 ; 
    const double zeta = f/C1 ; 


    // bisection search for root... 
    double a = 0.0 ; 
    double b = 1.e+5; 

    cout << setw(12) << func(a,zeta) << endl ; 
    cout << setw(12) << func(b,zeta) << endl ; 

    int iter = 0; 
    while(true) { 

      if ( iter > 100 ) { 
        cout << " Failed to converge ... " << endl ; 
        break;
      } 

      double c  = (a+b)/2.0 ; 
      double fa = func(a,zeta) ; 
      double fb = func(b,zeta) ; 
      double fc = func(c,zeta) ; 

      if ( fc*fa < 0.0 ) b = c  ; 
      else { 
        assert ( fc * fb < 0.0 ) ; 
        a = c ; 
      } 

      if ( fabs(a-b) < 1.0e-04 ) { 
        cout << " z = " << c << endl ; 
        cout << " F(z) = " << fc << endl ; 
        break; 
      } 
    }
    
    
    return 0;
}

//*********************************************************************//
double func(double z, double zeta) { 
  return integral(z) - zeta ; 
}

double integral(double z) { 

  gsl_integration_workspace *work_ptr
    = gsl_integration_workspace_alloc (1000);
  
    double error;
    double result;
    double rel_error = 1.0e-8;
    double abs_error = 1.0e-8;
    double upper_limit = z;
    double lower_limit = 0;
    
    gsl_function My_function;
    void *params_ptr = NULL;
    
    My_function.function = &my_integrand;
    My_function.params = params_ptr;

    gsl_integration_qags (&My_function, lower_limit, upper_limit,
        abs_error, rel_error, 1000, work_ptr, &result,
        &error);
    
    gsl_integration_workspace_free(work_ptr) ;
    return result ; 
} 


double my_integrand (double x, void *params) {
  return pow(x,4)/pow( (1.0 + pow(x,2)), 17./6.0); 
}
